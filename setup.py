# copyright: 2022 EUMETSAT
# license: LICENSE.txt

import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

setuptools.setup(
    name="eumartools-benloveday",
    version="0.0.1",
    author="Ben Loveday",
    author_email="ops@eumetsat.int",
    description="A package of tools for manipulating EUMETSAT marine Earth observation data",
    long_description="A package of tools for manipulating EUMETSAT marine Earth observation data",
    long_description_content_type="text/markdown",
    url="https://gitlab.eumetsat.int/eo-lab-usc-open/ocean/tools/eumartools",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>3.7',
)
