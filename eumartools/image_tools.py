"""
copyright: 2022 EUMETSAT
license: ../LICENSE.txt

An image tools module with functions for manipulating EUMETSAT marine data.

This module contains functions for manipulating EUMETSAT marine data. It is
part of the eumertools toolkit.

  Typical usage example:

  import eumartools

  grid_array = eumartools.reduce_array(grid_array, grid_factor=2)
"""

import numpy as np
from skimage import exposure
from matplotlib.path import Path

def histogram_image(grid_array, nbins=512):
    """Function to reduce the dynamic range of an array by histogram binning

    Args:
        grid_array (numpy array): two-dimensional array to be reduced
        nbins (int): number of bins to use in the histogram

    Returns:
        if successful, the histogrammed array
        else returns an error

    """

    try:
        return exposure.equalize_adapthist(grid_array, nbins=nbins)
    except Exception as error:
        msg = "Unsuccessful!", error, "occurred."
        print(msg)
        return msg


def reduce_image(grid_array, grid_factor=1):
    """Function to re-sample an image on a geometrically coarser grid

    Args:
        grid_array (numpy array): two-dimensional array to be reduced
        grid_factor (int): factor to reduce grid_array by in each dimension

    Returns:
        if successful, the reduced array
        else returns an error

    """
    try:
        img_array = grid_array.copy()
        return img_array[::grid_factor, ::grid_factor]
    except Exception as error:
        msg = "Unsuccessful!", error, "occurred."
        print(msg)
        return msg


def truncate_image(grid_array, min_percentile=5.0, max_percentile=95.0):
    """Function to remove image array outliers according to their values in a
        histogram distribution. Supports NaN in the input array.

    Args:
        grid_array (numpy array): two-dimensional array to be truncated
        min_percentile (float): the bottom percentage of values to remove
        max_percentile (float): the top percentage of values to remove

    Returns:
        if successful, the reduced array
        else returns an error
    """

    try:
        img_array = grid_array.copy()
        
        low_bound = np.percentile(grid_array[np.isfinite(grid_array)],
                                  min_percentile)
        high_bound = np.percentile(grid_array[np.isfinite(grid_array)],
                                   max_percentile)

        # lower bound
        img_array[grid_array < low_bound] = low_bound
        # upper bound
        img_array[grid_array > high_bound] = high_bound

        return img_array
    except Exception as error:
        msg = "Unsuccessful!", error, "occurred."
        print(msg)
        return msg


def normalise_image(grid_array, contrast=[1.0, 1.0, 1.0], unhitch=False):
    """Function to normalise an image using independant channels (unhitch=True)
        or combined channels (unhitch=False, default)

    Args:
        grid_array (numpy array): three-dimensional RGB array to be normalised
        contrast (list): non-linear (^2) channel scaling, e.g. [1.0 1.0 1.0]
        unhitch (bool): switch for independant or cross-channel normalisation

    Returns:
        if successful, the normalised array
        else returns an error
    """

    try:
        img_array = grid_array.copy()
        
        if unhitch:
            # normalise with separating channels
            for ii in range(np.shape(img_array)[-1]):
                minval = np.nanmin(img_array[:, :, ii])
                maxval = np.nanmax(img_array[:, :, ii])
                img_array[:, :, ii] = (img_array[:, :, ii] - minval) / (
                    maxval - minval
                )
                # apply contrast
                img_array[:, :, ii] = img_array[:, :, ii] ** contrast[ii]
        else:
            # normalise without separating channels
            minval = np.nanmin(img_array)
            maxval = np.nanmax(img_array)
            for ii in range(np.shape(img_array)[-1]):
                img_array[:, :, ii] = (img_array[:, :, ii] - minval) / (
                    maxval - minval
                )
                # apply contrast
                img_array[:, :, ii] = img_array[:, :, ii] ** contrast[ii]

            # non-linearity: contrast - note that the range is not between
            # 0 and 1, so need to renormalise afterwards
            minval = np.nanmin(img_array)
            maxval = np.nanmax(img_array)
            for ii in range(np.shape(img_array)[-1]):
                img_array[:, :, ii] = (img_array[:, :, ii] - minval) / (
                    maxval - minval
                )

        return img_array
    except Exception as error:
        msg = "Unsuccessful!", error, "occurred."
        print(msg)
        return msg


def point_distance(lon1, lon2, lat1, lat2, mode="global"):
    """Function to calculate the distance between a lat/lon point and lat/lon
       arrays.

    Args:
        lon1 (float): longitude coordinate
        lon2 (numpy array): longitude array
        lat1 (float): latitude coordinate
        lat2 (numpy array): latitude array

    Returns:
        if successful, array of distances
        else returns an error

    """

    try:
        R_earth = 6367442.76

        if mode == "global":
            # Compute the distances across the globe
            phi1 = (90 - lat1) * np.pi / 180
            phi2 = (90 - lat2) * np.pi / 180
            theta1 = lon1 * np.pi / 180
            theta2 = lon2 * np.pi / 180
            cos = np.sin(phi1) * np.sin(phi2) * np.cos(
                theta1 - theta2) + np.cos(phi1) * np.cos(phi2)
            dist = R_earth * np.arccos(cos)
        elif mode == "local":
            # Compute the distances with local approximation (no curve)
            dist = R_earth * ( (lon2 - lon1)**2 + (lat2 - lat1)**2 )**0.5
        return dist
    except Exception as error:
        msg = "Unsuccessful!", error, "occurred."
        print(msg)
        return msg


def subset_image(grid_lon, grid_lat, lons, lats, mode="global"):
    """Function to cut a box out of an image using the grid indices
        for the image corners. BEWARE USING THIS ON HALF-ORBIT,
        FULL-ORBIT or POLAR DATA.

    Args:
        grid_lon (numpy array): longitude array
        grid_lat (numpy array): latitude array
        lons (list): list of vertex longitudes
        lats (list): list of vertex latitudes
        mode (string): 'global' or 'local'

    Returns:
        if successful, returns ij indexes of box corners.
        else returns an error

    """

    try:
        nx = np.shape(grid_lon)[0]
        ny = np.shape(grid_lon)[1]
        poly_verts = []
        extracted_x = []
        extracted_y = []
        for lon,lat in zip(lons, lats):
            dist = point_distance(lon, grid_lon, lat,
                          grid_lat, mode=mode)
            i0, j0 = np.unravel_index(dist.argmin(), dist.shape)
            poly_verts.append((i0, j0))
            extracted_x.append(j0)
            extracted_y.append(i0)

        x, y = np.meshgrid(np.arange(nx), np.arange(ny))
        x, y = x.flatten(), y.flatten()
        points = np.vstack((x, y)).T

        path = Path(poly_verts)
        mask = path.contains_points(points)
        mask = mask.reshape((ny,nx)).astype(float)
        mask[mask == 0.0]=np.nan

        return extracted_x, extracted_y, mask
    except Exception as error:
        msg = "Unsuccessful!", error, "occurred."
        print(msg)
        return msg
