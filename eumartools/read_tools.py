"""
copyright: 2022 EUMETSAT
license: ../LICENSE.txt

A read tools module with functions for manipulating EUMETSAT marine data.

This module contains functions for manipulating EUMETSAT marine data. It is
part of the eumertools toolkit.

  Typical usage example:

  import eumartools

  grid_array = eumartools.read_manifest("xfdumanifest.xml", "centralWavelength", attrib=None)
"""

import xml.etree.ElementTree as ET

def read_manifest(xml_file,
                  match_string,
                  xmlns_tag="sentinel3",
                  xmlns_schema="http://www.esa.int/safe/sentinel/sentinel-3/1.0",
                  attrib=None):
    """Function to read elements from a SAFE-format xfdumanifest.xml file

    Args:
        xml_file (string): the manifest file
        match_string (string): the string to find
        xmlns_tag (string): the xmlns header tag
        xmlns_schema (string): the xmlns header schema
        attrib (string): the attrib to extract, if None then value is read
        
    Returns:
        if successful, a list of the mathching elements
        else returns an error

    """

    try:
        elements = []
        tree = ET.parse(xml_file)
        root = tree.getroot()
        match_dict = {xmlns_tag:xmlns_schema}
        
        for item in root.findall(f".//{xmlns_tag}:{match_string}", match_dict):
            if attrib:
                elements.append(float(item.attrib[attrib]))
            else:
                elements.append(float(item.text))
        
        return elements
    except Exception as error:
        msg = "Unsuccessful!", error, "occurred."
        print(msg)
        return msg

