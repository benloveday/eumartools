# copyright: 2022 EUMETSAT
# license: ../LICENSE.txt

import unittest
import xarray as xr
import sys
sys.path.append("..")
import eumartools


class TestTools(unittest.TestCase):
    
    def test_flag_tools(self):
        applied_flags = ['CLOUD', 'CLOUD_AMBIGUOUS', 'CLOUD_MARGIN',\
                 'INVALID', 'COSMETIC', 'SATURATED', 'SUSPECT',\
                 'HISOLZEN', 'SNOW_ICE', 'AC_FAIL', 'WHITECAPS']
        self.assertEqual(eumartools.flag_mask('wqsf_test.nc', 'WQSF', applied_flags,
          test=True), 10201)
        
    def test_read_tools(self):
        self.assertEqual(eumartools.read_manifest('xfdumanifest.xml', 'centralWavelength'), 
          [400.0, 412.5, 442.5, 490.0, 510.0, 560.0, 620.0, 665.0, 673.75, 681.25, 708.75, 753.75, 778.75, 865.0, 885.0, 1020.0])

    # add tests for image tools
    
if __name__ == '__main__':
    unittest.main()