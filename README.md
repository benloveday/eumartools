# eumartools

This repository contains the source code for eumartools, a library of python routines for working with EUMETSAT Copernicus Marine Data Stream products

## License
 
This code is licensed under an MIT license. See file LICENSE.txt for details on the usage and distribution terms. No dependencies are distributed as part of this package. Copyright EUMETSAT 2022.

All product names, logos, and brands are property of their respective owners. All company, product and service names used in this website are for identification purposes only.

## Authors

* [**Ben Loveday**](mailto://ops@eumetsat.int) - [EUMETSAT](http://www.eumetsat.int)

Please see the AUTHORS.txt file for more information.

## Prerequisites
 
To build the package from source, you will require conda-build and setuptools. These are not
required to use the package.

## Dependencies

|item|version|licence|package info|
|---|---|---|---|
|python|3.8.13|PSF|https://docs.python.org/3/license.html|
|conda-build|3.21.7|BSD3|https://anaconda.org/conda-forge/conda-build|
|setuptools|58.0.4|MIT|https://anaconda.org/conda-forge/setuptools|
|xarray|0.20.2|Apache2|https://anaconda.org/conda-forge/xarray|
|netcdf4|1.5.4|MIT|https://anaconda.org/conda-forge/netcdf4|
|scikit-image|0.19.1|BSD3|https://anaconda.org/conda-forge/scikit-image|
|numpy|1.22.2|BSD3|https://anaconda.org/conda-forge/numpy|
|matplotlib|3.5.1|PSF|https://anaconda.org/conda-forge/matplotlib|
